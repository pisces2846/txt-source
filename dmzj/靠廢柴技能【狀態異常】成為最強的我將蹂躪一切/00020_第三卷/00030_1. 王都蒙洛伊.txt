往密集的建築上方一望，晴空萬里。

我們行經烏爾薩的王都蒙洛伊。
異世界奇幻風格強烈的城下町。
我第一次見到這個世界的巨大都市。
在亞萊昂，我只見過室內的風景。

⋯⋯因為我直接就從城內，被送往了廢棄遺跡。

嗶嘰丸輕聲叫著：「噗啾咿～」

「你要躲好喔。」
「嗶。」

長袍下的嗶嘰丸就像平常一樣。
與黑龍騎士團一戰後，我們花了數天抵達蒙洛伊。
現在還沒有追兵的氣息。
到這裡為止，我們路過三座村莊。
一路上，從旅人和傭兵那裡聽到了那項傳聞。

五龍士之死，以及消失的瑟拉絲・亞休連。
這兩個話題，我聽到了不少，但能得知的也僅止於此。
各國的反應與巴庫歐斯的動向都還不清楚。
在這個王都，或許還能獲得不一樣的情報。
我回頭看往王都的大門。

我們輕鬆通過了大門。沒有接受到類似盤查的檢查。
也沒有瘋狂搜索瑟拉絲的氣氛。
覆滅的是其他國家的騎士團。

因此站在烏爾薩的立場，或許對搜捕犯人沒有那麼感興趣。
又或者是，我的偽裝發揮超出預期的效果了嗎？

「對烏爾薩而言，鄰國那支危險的騎士團消失，他們可能還鬆了一口氣呢。」
「有可能。」

走在我後方的瑟拉絲說道。
現在，她的容貌改變了。名字也使用假名「彌絲拉」

「據說烏爾薩的魔戰騎士團，比起他國的主戰力，實力稍微差了一些。當然，如果對上黑龍騎士團，就必敗無疑吧。」
「能夠存活至今，是多虧了女神的口才嗎？」
「我想也有可能，不過現在烏爾薩有名為『飛龍殺手』的男人。得到他，烏爾薩就能向他國展示實力。因為即使沒有戰爭，也必須誇示國力才行。」

也就是用他來抑止別國動歪腦筋。就算如此──僅僅一個男人，就有如此龐大的抑止力嗎？

「但是，就連那位飛龍殺手也無法驅逐五龍士啊。」
「雖說是飛龍殺手，但我想他還是贏不過屍人。雖然其他五龍士都躲在屍人的背後，但他們也都擁有出眾的實力⋯⋯再加上，我也聽說過飛龍殺手天生就怕麻煩。不管怎樣，他都會避免跟五龍士發生衝突。」

想必一定是個非常怕麻煩的人吧？言歸正傳──

「首先，先去找今天住宿的地方吧。」

我們只會在蒙洛伊停留一天。
以便做好準備進入魔群帶。
我看看背袋。
裡頭放著《禁術大全》

這本書裡也記載了各種藥物的製法。在魔群帶可能找得到要用的材料。
因此要備好製作用的道具。這樣就算在魔群帶中，也能製作藥物。
這裡是王都。應該會有米魯茲所沒有的道具吧。
這也是我的目的之一。

「這間旅社好像不錯。」

尋找的結果，我們選了一家價格還算便宜的旅社。進去前，我問瑟拉絲：

「房間分開，可以吧？」

王都裡到處都是人。
米魯茲是一座小都市，小到守衛都可記住旅客長相的程度。
附近一帶往來的人數也不算多。
因此，在米魯茲走動會很顯眼。
所以，「可疑的二人組」會更加醒目吧。
不過，這裡是人多混雜的王都。應該也會有許多外地人造訪此處。

現在瑟拉絲改變了容貌和服裝。
烏爾薩也沒有要認真搜查那件事的跡象。
似乎不用像在米魯茲附近時那樣警戒。
所以，一起行動也不會有問題。我這麼判斷。

只不過，瑟拉絲是女人。
和男性同住一個房間也不方便⋯⋯

房間還是應該分開吧。這點顧慮，我還是有的。
我是這麼想，然而──

「我想，住一間房比較明智，這樣也不會浪費旅費。只要主人不討厭的話。」
「我好歹是個男的耶？」

瑟拉絲愣住，接著把手伸向嘴邊，好像在思考什麼。
然後，瑟拉絲清清喉嚨，她似乎理解了我話中的意思。

「我沒問題。」

瑟拉絲把手放在胸口。

「我們在米魯茲遺跡時，已經在同一間小房間裡睡過了，我認為沒有什麼問題。」

因為那時，我讓嗶嘰丸吸引她的注意力，對她用了【SLEEP（睡眠性賦予）】啊！

「⋯⋯妳不在意的話就好。」
「也不是完全不在意⋯⋯不過，如果是你，我沒問題。」

也罷，叔叔也老是告訴我，節儉就是從小地方累積。

「那就住一間房吧。妳之後要是有什麼怨言，我可不管喔。可以吧？」
「是、是的。」

瑟拉絲小跑步跟在後頭。
她對信賴的對象，果然就沒什麼戒心。
至於發誓效忠的對象，就更不用說了。

⋯⋯但她的戒心會降到這麼低，還真是出乎我意料之外。

我們進入旅社。
叫住老板，辦理手續。
寫好住宿登記手冊後，看了一眼登記內容的老板，看著我們的臉問：

「你們兩位是傭兵吧？」
「是的。雖然我們還沒去傭兵公會登記。」

我聽說有很多沒有登記的傭兵。這種說法應該不會讓人起疑。

「你們來蒙洛伊的目的，果然是血闘場嗎？」
「那是目的之一。」

當然是假的。
關於血闘場的事，我事先聽瑟拉絲說過了。
是奴隷及傭兵互相殘殺的競技場。應該就像是古代羅馬競技場的感覺吧。
據說參戰者被稱為「血闘士」

瑟拉絲是這樣說明的。

『血闘場現在主要提供民眾娛樂之用，原本是傭兵團選拔評定入團戰士的場地。基本上，聽說現在還留有遴選參戰者、提拔為傭兵的文化。』

所以，似乎還滿多傭兵以觀賞血闘為目的來到蒙洛伊。

『只要在比賽中獲勝，就能得到報酬。因此，好像也會有滿多為生活所苦的傭兵出場呢。受觀眾歡迎的奴隷血闘士，對擁有他們的主人來說，也是很好的賺錢工具，因此他們不會被賣給傭兵團，而是作為最受歡迎的戰士，被硬留在血闘場。』

血闘場的營運母體有兩個。
據說是烏爾薩公爵家，及傭兵公會。
話說回來，找瑟拉絲當伙伴真是太正確了。
這世界上大致的基礎知識，她都知道。
她就是個會走路的人形字典──不，應該是精靈字典。

「傭兵公會的足跡遍佈得真廣啊。」

我望著書狀標識的招牌。
現在我們出了旅社，走在大街上。

「公會是跨國進行活動的。對來往各國的傭兵來說，是很可靠的組織。其中，尤其是傭兵公會和魔術師公會，在各國都擁有不小的影響力。」

傭兵公會嗎？我記得他們好像也參與了探索米魯茲遺跡。
我們首先到了道具店。
盡可能在那裡買齊方便攜帶的器具。
瑟拉絲這次殺價也殺得很漂亮。
出了道具店後，我摸摸懷裡的小袋子。裡面放著青龍石。
要換錢嗎──還是算了。
一把青龍石放到市場上，話題就會立刻傳開。當然，拿出來賣的人也會成為話題吧？
我想避免無意義地引人注目。
我束緊小袋子的袋口。所幸，荷包依舊充足。
從廢棄遺跡的那對骸骨處得到的金錢。
從追逐瑟拉絲的四人組身上得到的金錢。
有人高價收購了我在米魯茲遺跡獲得的寶石＆裝飾品。
合計起來，數目可不小。
金錢上充裕無虞。不用擔心旅費，實在值得感謝。
雖說如此，還是必須節儉。

「妳還是一樣這麼會殺價呢。」

殺價時的瑟拉絲不可小覷。
不露骨脅迫，且身段柔軟。
但是，不能妥協的地方，她絶對不會讓步。
瑟拉絲苦笑說：

「我生性小氣呀。」
「應該說是勤儉。妳沒必要貶低自己。」

瑟拉絲輕笑。

「我的主人很會拉攏部下的心呢。」

她好像已經徹底把自己視為我的部下了。

「對了，我想調查看看在米魯茲遺跡發現的黑蛋。」

據說蒙洛伊有公開書庫。簡單來說，就像是國家圖書館一樣的地方吧。

「任何人都能自由進出那裡嗎？」
「我記得蒙洛伊的公開書庫──」

瑟拉絲喚起記憶。

「若是未經認可的王都民眾，必須取得許可證才行。有效日僅限一天。」

關於黑蛋，連精靈字典瑟拉絲也一無所知。
表示被記載在公開書刊中的機率也就很低，是嗎？

「那顆蛋的事，我想，去問目的地的那個人比較好。」

如果是禁忌魔女，或許知道些什麼。

───

之後，我們決定去酒館吃晚餐。
去酒館也可順便蒐集情報。喝到茫然忘我的地方，是獲得最新話題的絶佳場所。

我們兩人進入一家大酒館。
首先點了香草水跟菜餚。雖然我們不喝酒，不過也點了酒。
為了融入此處。

因為是身在異世界，所以我或許可以不用管什麼未成年之類的法律。
只是，因為親生父母的關係，我對酒沒什麼好印象。
因此我無法喜歡酒。就算我知道酒本身並沒有罪過。

順便一提，瑟拉絲好像會喝一點。但似乎也不是很喜歡。
我一邊吃飯，一邊豎耳聆聽。
我聽見鄰近座位的對話⋯⋯

「喂，你聽說了嗎？」
「嗚咦咦～⋯⋯嗯？聽說什麼？」
「黑龍騎士團的事啊。」
「又是那件事。」
「不是不是。是有關消滅他們的人啦。」
「喔？有傳出什麼新消息了嗎？」
「好像是從王城來的人傳出來的消息喔？」
「呵呵，那似乎滿可信的耶。」

就是所謂「根據相關人士透露」的，是嗎？

「原本被認為是殲滅五龍士的最大嫌疑者──瑟拉絲・亞休連，好像已經死了。」

瑟拉絲的表情突然變得很痛苦。

「唔嗚！」

她好像被食物噎住了。

「唔嗚、嗚⋯⋯」

我把水遞給她。

「妳還好嗎？」

瑟拉絲喝下水，呼了口氣。

「謝謝⋯⋯真、真是非常抱歉。」

突然在這種地方聽說自己死了。突如其來的消息，一定嚇到她了吧？
那幾個男人並未注意到我們，繼續說。

「她和五龍士同歸於盡了嗎？」
「不，瑟拉絲・亞休連好像是打輸戰死的。」
「什麼？那是誰殺了五龍士？」
「嘿嘿，你果然還不知道。殺了五龍士的，據說是亞信特。」

亞信特？

「啊！就是最近傳聞中的咒術師集團嗎！」
「他們宣稱五龍士是在咒術下喪了命。」

我和瑟拉絲交換眼神，壓低聲音。

「妳怎麼想？」
「最近，我聽說過各地流傳著咒術的傳聞。」
「他們說的咒術很常見嗎？」
「不，並不常見。」

瑟拉絲輕輕說明。
術式與咏唱咒語。一般世人知曉的是這兩項。
硬要加上第三項的話，應該就是勇者的「技能」吧。

「那精靈術呢？」
「嗯，精靈術是──」

據瑟拉絲所說，咒術並不怎麼常見。
這麼說來，米魯茲酒館的客人也只有模糊的認知。
不過，能使用精靈術的精靈，似乎本來就是很稀有的存在⋯⋯

咒術是最近才傳開來的。
散佈傳言的人，是名為亞信特的咒術師集團。

「我聽說他們是崇拜咒神的集團。」
「簡單來說，他們是想借此將自己的咒術賣給某個地方吧⋯⋯」

他們認為這說不定是個好機會，所以主動承認自己是殺了五龍士的犯人。
這種謊言遲早會暴露吧？不過，應該多少能打亂搜索犯人的步調。
只是，我並不清楚異世界的情報網發展到什麼程度⋯⋯

反正我們不久之後就要進入魔群帶。只要在那之前能多爭取到一些時間就好。
至於說到那個亞信特，現在在哪的話──

「我們是亞信特的守護者！」

酒館的門被大大推開。

「我們是守護著咒神落入凡間之子──慕亞齊大人的咒兵！好了，請讓出位子！」

穿著紫色長袍的集團一個接一個走進來。
就是剛才提到的咒術師集團。
他們似乎就在烏爾薩。

「喔？」

我轉動湯匙舀起湯。

「那就是咒殺了五龍士的咒術師集團嗎？」

店內大桌的客人們讓出座位。
對於要他人讓座，亞信特成員們絲毫不覺得抱歉，魚貫坐下。

「快點拿酒來！我們可是烏爾薩的救世主喔！」

傳聞是傳聞。
自稱是自稱。
可是，也不能說它完全是謊言。
或許他們真的殺了五龍士。
沒有人能斷定他們不是──沒錯，除了在現場的人以外。

⋯⋯不論如何，他們還真是堂而皇之。

完全沒有鬼鬼祟祟的感覺。
最強騎士團的毀滅。
如果他們真的是做出這件事的元凶，國家應該會採取動作才對，可是⋯⋯
難道，烏爾薩已經拉攏他們了嗎⋯⋯？

酒客們散發出不想和他們有所牽扯的氣氛。
我試著暗中觀察亞信特那群人。
就現在看來，他們並不如屍人那般充滿壓迫感。確實沒有。
就我所見，他們僅僅是來酒館飮酒作樂而已。

⋯⋯暫時放著不管也行吧？

我希望他們能夠繼續作為我的幌子，再撐一陣子。
酒館客人相繼回到原有的步調。
我喝完水，對瑟拉絲說：

「把剩下的菜吃完，我們就回旅社吧。」

我大致上記住走進店裡的亞信特成員長相了。

「知道了。請稍等我一下。」

我等著瑟拉絲吃完。
她食量不小，但都小小口地吃。因此吃的速度很慢。
瑟拉絲連忙將菜往嘴裡送。

「⋯⋯妳可以慢慢吃。」
「非、非常抱歉。啊唔⋯⋯嚼、嚼⋯⋯」

等待的期間，鄰座的人又開始聊起來了。

「那些人就是傳聞的『黑龍殺手』嗎？」
「果然是這群人咒殺的吧？」
「不過，就算是其他人殺的，他們只要說『那都多虧了我們的詛咒』，別人就無法反駁了啊。」

原來如此。還能如此解讀啊。

「假如是其他人殺的，但對手可是五龍士耶？還有誰辦得到？那些傳說中的名人強者，當時都不在烏爾薩，對吧？啊，死掉的瑟拉絲・亞休連剛好跟五龍士同歸於盡的說法，也說不通了。」
「唔嗚嗚嗚！」

瑟拉絲痛苦地搥胸。
她好像還不習慣突然聽到自己的名字出現。關於這點，她倒是微妙地漏洞百出。
男人們繼續聊著。

「現在還沒人提過，又最有可能打贏五龍士的，就屬血闘場的豹人了吧？」
「啊～她確實是很厲害的怪物。」

血闘場裡好像也有知名人物。

「啊，對了，禁忌魔女已經被提過了嗎？」
「相傳居住在大遺跡帶的那個魔女嗎？我記得她好像是黑暗精靈吧？」
「聽說她會用超級強大的魔法，不是嗎？如果是禁忌魔女，應該就能殺了五龍士吧？」
「不過，已經有十年以上沒人見過她的踪影了吧？畢竟她住在那個可怕的大遺跡帶，所以連是不是還活著，都沒人知道呢──」
「其實我聽說過，見過那位禁忌魔女的傢伙，似乎就在王都裡喔。」

嗯？

「而且，據說那個人連魔女住在哪裡都知道。」
「是指她住在大遺跡帶裡的某處嗎？那種事我也知道啊。」
「不是啦。那個人連魔女住在大遺跡帶的『哪裡』都知道。」
「那傢伙是魔女的熟人之類的嗎？」
「這我就不清楚了⋯⋯不過，你們聽了可別嚇到囉？不瞞你們說，其實這個人就是──」
「呵嘿嘿，我對那種事沒興趣。連是不是還活著都不清楚的黑暗精靈，怎樣都好啦。」

另一個男人打岔。

「比起那個，阿布羅姆的妓院，好像來了一個長相和穿著都跟亞萊昂女神相似的妓女喔？」
「喔喔？然後呢？」
「不過，聽說實際上似乎長得一點都不像，子安公爵一怒之下當場就斬殺了那名妓女！」
「哇哈哈哈！所以，才鬧得天下皆知啊！」
「還有，公爵大人後來好像又要求，要找長得像約納特聖女的妓女──」

話題歪了。我想知道認識禁忌魔女的人是誰啊。
我抓了幾枚銀幣，站起身來。

「嚼嚼⋯⋯主人？」
「我過去一下。」

我將手伸向男人那桌的空位。

「抱歉。這個位子，我可以一起坐嗎？」
「啊？小兄弟你想幹嘛？」
「抱歉，冒昧打擾。那個見過禁忌魔女的人，我很想繼續聽聽他的事。」
「啊～？」

提起妓女話題的男人顯得不太開心。

「你是什麼東西啊？突然闖進來──」
「啊，在那之前，讓我請你們一人喝一杯最喜歡的酒⋯⋯不，一人兩杯。然後每種菜都各點一份。當然，這個也算我的。」

一臉不滿的男人，表情為之一變。

「──哇、哇哈哈哈！對了、對了！禁忌魔女的事對吧！抱歉抱歉！不小心就打斷話題了！說實話，我也對那件事很感興趣呢！」

另一個人也附和。

「不錯嘛，少年！滿足求知的好奇心，就是年輕人最美妙之處啊！好，大叔我就告訴你吧！喂，小姐，同樣的酒再來一杯！」

這次換成男人們邀我坐下。要融入他們比想像中的還容易。

「曾經見過禁忌魔女的人，好像是被譽為蒙洛伊最強的血闘士。她名叫伊芙・史畢德。」

血闘士嗎？

「那位伊芙所擁有的情報，跟我們常聽到的『知道魔女住在金栖魔群帶』有什麼不一樣？」
「她連魔女住在魔群帶的哪邊都曉得。她好像曾經向血闘士戰友透露過一次。」
「也有可能是假消息吧⋯⋯？」
「或許吧！不過，我都說了，你還是得請我們喝酒喔～？」
「是的，當然。」
「好！我中意你！」

男人們對彼此哈哈大笑。

「⋯⋯⋯⋯」

禁忌魔女似乎是隱居在金栖魔群帶裡的黑暗精靈。
至今我獲得的情報大概就這些。
對了⋯⋯去見那個血闘士一面也是個辦法。

「要怎樣才能見到那個伊芙呢？」
「去血闘場不就行了嗎？那位怪物好像生活在那裡吧？不過，她現在也會像一般人一樣在這附近走動。如果你能跟掌管血闘場的子安公爵說上話，應該就能好好與她見上一面了。」
「這樣啊，謝謝。那我就差不多告辭了──」

旅館大門的方向突然喧騰起來。接著，旁邊的男人把手搭在我肩膀上。

「太好了。少年你運氣不錯。」

來者身穿皮革制的輕鎧甲。身上配著劍。
身材精瘦結實。痩雖瘦，但似乎有肌肉。
只是，一開始吸引我目光的是其他部位。

豹頭。

不是那種人類姿態加上一些野獸元素的感覺。
我在網路上看過豹的頭。對方的頭部大概就是那樣的感覺。
雙腳步行的人類。黃、黑、棕色的體毛。

───

豹人──這樣形容好嗎？從身材來看，好像是個女人。

「哇哈哈，你是第一次見到豹人吧？也難怪，因為是很罕見的種族嘛。難怪你會驚訝。」

一旁的男人「啪」一聲拍了我的肩膀。

「那就是蒙洛伊最強的血闘士，伊芙・史畢德。」

豹人在吧台的座位坐下。兩邊座位都空著。
我觀察酒館老板的反應，他的舉止就像在對待熟客。
我大概也確認了一下亞信特成員的模樣。那群人在裡面的位子喝得很開心。
他們並未特別在意豹人。態度感覺並不好，但是⋯⋯

看起來好像早就已經看慣了豹人這種存在。
可以說，整間店都是如此。只有「看慣的名人來到店裡了」那種程度的反應。
只不過，沒有人去搭話。

「我直接去問她本人，看看剛才我們說的禁忌魔女的消息是不是真的。」

我對同桌的男人說。男人笑一笑，放開我的肩膀。

「年輕人真是大膽啊。好，我們差不多該走了⋯⋯今天比平常喝得多，有點醉了。我想呼吸一下外面的空氣⋯⋯嗚咿咿～」

同桌的兩人組離席，跌跌撞撞地走出酒館。
我回到瑟拉絲那裡，說完悄悄話，我拿出銀幣。

「那，我走囉。」
「如果有必要，我會馬上介入。」
「雖然我不打算要惹事，不過一旦發生什麼，就拜託妳了。」

我想盡量避免在這裡使用技能。
這種時候就要仰賴瑟拉絲的戰鬥能力。

「拜託妳囉。」

瑟拉絲一臉嚴肅地將手舉在胸前。

「包在我身上。」

我走向吧台。
我在豹人旁邊的座位坐下，點了杯香草水。然後，在豹人的前面放了一枚銀幣。

「我能請妳吃點什麼嗎？」

豹人只移動視線看向我。她稍作沉默後──

「你想幹嘛？」

她問我。能用人話溝通，沒有障礙。
豹人的聲音強而有力。而且，還很沉穩。
聲音不如想像中粗獷。非但如此，而且是清澈透亮的聲音。

「我聽到妳的傳聞，很想見妳一面。我叫哈提，是個傭兵。」
「我叫伊芙・史畢德。」

報上名字後，伊芙凝視了我一會兒。

「你第一次見到豹人嗎？」
「對。」
「不過，你的反應卻不像看到很稀奇的東西。」

我不能草率回答。因為我不知道什麼地方是她的地雷⋯⋯

我笑著回答：

「我在旅途中見過各式各樣的事物。雖然可能有點失禮，但豹人還不至於讓我吃驚啊。」

別說豹人了，廢棄遺跡裡甚至有人形雙頭豹。
其他還有很多合成獸之類的東西。和那些魔物相比，豹人帶給我的驚訝就不算大了。

撕咬！

伊芙既豪邁又爽快地咬下帶骨肉。
她舔了舔殘留在手掌上的肉汁。同時，一雙貓眼凝視著我。

「真是豪邁的吃相啊。」
「哼～你沒有把我當野蠻人看待嗎？」

她是故意用那種吃法給我看的。
有種被試探的感覺──看來，她大概是想看看我會有什麼反應。

「我也稱不上是有氣質的人。而且用餐時比起注重禮儀形式，能吃得津津有味才是最棒的。啊啊，這麼說來血闘士──」
「說，你的目的是什麼？」
「⋯⋯⋯⋯」
「你感興趣的，不是我血闘士的身分。我沒說錯吧？」

我露出困惑的笑容。

「被、被妳發現了啊⋯⋯果然名不虛傳呢⋯⋯」

雖然我也是故意製造出那樣的氣氛，但是她能注意到真是太好了。

「你找我有何貴幹？」

嗯？她願意聽我說話嗎？
我畢恭畢敬地詢問。

「聽說妳知道禁忌魔女在哪⋯⋯」
「魔女在金栖魔群帶。任何人都知道這個傳聞吧？」
「但我聽別人說，妳曉得魔女在魔群帶的『什麼地方』⋯⋯」

伊芙的喉嚨深處發出低沉的笑聲。

「連你也相信這種不切實際的傳聞？」
「也就是說，只是空穴來風嗎？」
「我確實去過魔女居住的金栖魔群帶。但是，並未見過她。」
「可是，聽說妳告訴過血闘士同袍，妳知道魔女的住所。」
「⋯⋯我在魔群帶生死徘徊了兩個星期。我能活著回來，說不定是禁忌魔女施展魔法出手相救──我記得以前也向其他血闘士說過這樣的玩笑。」
「原來如此，是玩笑話以訛傳訛⋯⋯也就是說，妳並不知道住處在哪？」
「很抱歉，正是如此。」

我失落地垂下雙肩。

「這樣啊。」
「抱歉無法回應你的期待。不過，這就是事實。」

伊芙把肉吃得精光。她邊擦手邊問我。

「你為何要尋找禁忌魔女？」
「大概是探究的精神吧。我將來的夢想是成為學者。」

我將今天買到的部分器具擁在懷中。
我將器具拿給伊芙看。
這是立志成為學者的人會攜帶的道具。雖然只是我的想像就是了。

「我想在蒙洛伊僱用傭兵，前往魔群帶見見魔女。說不定還能發現未知的植物──」
「我勸你還是放棄比較好。」

伊芙像是在警告似地打斷了我的話。

「那裡不是一般人進入後還能活著出來的地方。連人稱蒙洛伊血闘場最強的我，都撐不到半個月了。我知道這樣說很沒禮貌，但你根本活不過三天。」

我低頭微笑，帶著感謝之情。

「伊芙小姐妳在擔心我的安危嗎？謝謝妳。」

伊芙露出有些意外的反應。就算是豹臉，表情也相當豐富。

「哼。」

伊芙目瞪口呆地嘆了一口氣。

「你真是個濫好人啊。」

我害羞地搔了搔頭。

「大家常這麼說。」

確實經常被這麼說。只不過，是在叔叔嬸嬸領養我之後的事情了。

「你外表來看起來還很年輕。要好好珍惜生命。」

伊芙拿起我放在桌上的銀幣，並將銀幣擺在我面前。

「雖然如此⋯⋯血闘士就好比拿生命當賭注的代名詞，我來說這種話，或許一點說服力都沒有吧。」

留下這句話後，伊芙走出酒館。
我收好銀幣，離開座位，望著伊芙身影消失的酒館大門。

⋯⋯好像有點相似。

然後，我叫住身後的「她」

「彌絲拉。」

這是瑟拉絲目前使用的假名。
我和伊芙對話時，她一直靠在吧台座椅附近的梁柱後面待命。

「走吧。」
「是。」

我們離開酒館。稍微走了幾步，又停了下來。

「對於剛才伊芙・史畢德所說的話，妳有什麼看法？」

首先最需要厘清的是──

一開始就該搞清楚是真是假。
沒錯，也就是──

「她在說謊。」
『妳真的知道禁忌魔女的住處在哪裡嗎？』

對於這個問題，伊芙的回答是「不知道」

瑟拉絲・亞休連能夠分辨謊話。
瑟拉絲依照指示，靠在附近的梁柱後面，聽著我們談話的內容。
為的就是判斷真假。

「因為她否認，所以反過來證明了她的確知道。」

也就是說──

「伊芙・史畢德清楚禁忌魔女在哪裡。」

我和瑟拉絲一起回到旅館。

「嗶啾～♪」

嗶嘰丸從床底下出現。
此時我注意到了。

「糟糕。」

瑟拉絲正準備將劍立直在牆邊，她問我：

「登河大人，你怎麼了？」

咒術師集團、豹人血闘士、禁忌魔女⋯⋯

因為接踵而來的事情使我陷入慌亂，忘了一件最重要的事。

「我忘記帶嗶嘰丸的食物回來了。」
「嗶啾咿～」

嗶嘰丸左右激烈搖晃著。仿彿在搖頭似地。
牠好像在跟我說「沒關係喔～」

我托著下巴。

「只好久違地用用那個了⋯⋯」

我從背包裡拿出皮囊，瑟拉絲露出不可思議的眼神。

「那個是⋯⋯皮囊，對吧？」

我把魔素注入寶石，皮囊散發出淡淡的光芒。

「這個啊，是能證明我是異世界人的手段之一。」

「唔啾啾～♪」

吃飽的嗶嘰丸身體變成淡粉紅色。
這次出現的是起司塔。
一半給了嗶嘰丸，剩下的由我和瑟拉絲平分。
因為是嗶嘰丸表示要這樣做的。
這只史萊姆果然細心得非比尋常⋯⋯

「烤得偏硬的面皮和⋯⋯這是起司嗎？」

瑟拉絲聞著香味。異世界沒有起司塔嗎⋯⋯？

瑟拉絲戰戰兢兢地將起司塔放入口中。
我也吃了一口。
稍硬的餅皮。起司的部分濃厚又甘甜。
起司軟硬適中。嚼勁恰到好處。接著傳來清淡的檸檬香。
餅乾和起司在口中融合⋯⋯

這個濃稠的奶油起司好吃極了。同時享受餅乾和起司硬軟不同的兩種口感，也令人開心。

「⋯⋯哎呀，我得小心別把餅乾屑撒到床上去了。」

不過其實也沒差，反正嗶嘰丸會幫我清掃乾淨。
瑟拉絲吃完後，雙手摀著臉頰。

「──太好吃了。」

她眼裡滿是驚訝。

「登河大人。」

強而有力的語氣。瑟拉絲跪在床上，探出頭，上半身整個往前靠。

「這到底是什麼東西？這在你的世界裡，是普通的食物嗎⋯⋯？」
「⋯⋯是沒錯。」

這積極主動的態度是怎麼回事？簡直就像是要把我壓倒的氣勢啊。
瑟拉絲回過神來，立刻往後退回身子。
她清了清喉嚨，一邊整理好她的衣䙓，一邊端正姿勢。

「失、失禮了。」
「妳喜歡異世界的食物嗎？」
「⋯⋯是的，好吃到讓人忍不住會心一笑。」
「我很想再讓妳多吃一點，但很可惜，我不能隨心所欲地拿出一樣的東西。我也不知道會變出什麼。而且還得再等一段時間才能使用。」

瑟拉絲接下我遞出的皮囊。

「這是勇者的異界道具，對吧？」

看來她知道勇者特殊裝備的存在。
我向她說明皮囊的效果。

「如果沒有這個皮囊，我恐怕無法從廢棄遺跡中生還。因為那裡既沒有食物，也沒有水。」

只不過，這次並沒有出現飮料。

「我第一次聽說這樣的異界道具⋯⋯因為據我所知，它是用來提升勇者活動狀態的裝備。」

瑟拉絲拿著起司塔的包裝，並仔細觀察。

「這是透明的袋子⋯⋯？上面記載的是──文字嗎？到底是用什麼材料和方法製作出來的呢？」
「是我們世界的特殊技術。擁有那些技術的是企業之類的機構，我是做不出來的。」
「喔喔⋯⋯企業是嗎？」

瑟拉絲感到稀罕地不停觀察起司塔的包裝。
正如我所預料，包裝紙可以用來證明我是異世界的人。

「登河大人。」
「嗯？」
「這個真漂亮呢。」

她覺得這個塑膠包裝很漂亮嗎？
另一方面，我看著眼前這個高等精靈，也覺得很漂亮。

「或許愈是自己世界沒有的東西，就愈顯得美麗吧。」

不過⋯⋯我之所以覺得瑟拉絲漂亮，可能不止因為她是高等精靈這麼簡單。
我抱著嗶嘰丸到床上坐下。

「好，我們來思考一下今後的方針吧。」

禁忌魔女的住處。即使是大致上的方位也好。
只要知道在哪裡，就可以縮短許多時間。比起胡亂尋找，可以省下好幾倍的時間。

「如果有方法能從那個血闘士口中問出情報就好了。」

若想靠技能解決，感覺還挺困難的。
讓她睡著後，再施以催眠術，讓她從實招來。
如果辦得到的話，倒另當別論⋯⋯但目前並沒有抓住她拷問這個選項。

「如果伊芙・史畢德是跟某個叫薇希斯（混帳女神）的一樣的混帳，說不定事情就好辦了。」
「要不要先去蒐集她的情報？」
「就這麼辦吧。」
「如果能知道她成為血闘士是為了什麼目的，說不定也比較好擬定方針。我聽說血闘士的動機也有很多種⋯⋯有為了賺錢才一直當血闘士的、有以活命為優先，同時等待被傭兵團挖角提拔的、有用獲得的獎金和主人交易，換取自由之身的。」
「也就是說，我們得視她的目的改變談判方法嗎？如果能找到具有效果的談判籌碼，就能好好利用了。」

雖然我很想知道禁忌魔女的住所位於何處，但我實在不想在蒙洛伊久留。
我想在短期間內解決這件事。

「⋯⋯瑟拉絲，我想再問妳一件事。」
「請儘管發問。」

我想到一個好點子。是因為剛才瑟拉絲的說明，才注意到的。
不論哪一種血闘士，都有個共通點。
雖然會依伊芙的狀況而異⋯⋯但說不定出乎意料地，很快就能解決了。

「血闘士除了性命之外，最重視的是什麼？」

有時，甚至是不惜賭上性命也想獲得的東西。

「我想大部分是錢。自願參與血闘的人，目的也是錢。想要被傭兵團挖角的人，或是想要金盆洗手的人，只要有錢，基本上就能動用從主人手中『買回自己』的制度了。」

血闘士可以用錢換取自由之身。據說在蒙洛伊的血闘場裡，有這樣的制度。
換句話說，就是一種「贖身」吧。
多虧這制度，想要成為血闘士的奴隷不計其數。
因為這樣就有機會從被束縛的身分中逃脫出來了。他們懷抱著這樣的一絲希望。
這個希望也提升了他們血闘時的動機。
血闘氣氛隨之變得熱血沸騰。如此必然就會增加賣點。
觀眾們看得開心。成為話題後，就能招攬更多觀眾。
也是一種對營運單位非常有利的制度。

「但是，血闘士要換取『自由之身』，還是需要相當龐大的金額。」
「龐大的金額，是嗎？」

我將手伸入懷中，緊緊握著裝有青龍石的袋子。

「我一直認為這塊寶石太引人注目，可以的話，實在不想這麼做，但是⋯⋯」

⋯⋯我手上正好有一個適合拿去變賣，以換取「龐大金額」的東西。


◇【瑟拉絲・亞休連】◇

「──好了。」

瑟拉絲・亞休連捧著放滿衣物的竹籃，前往旅館的洗衣場。
她繞到一樓的後門，那裡有一間屋頂凸出的小屋。
就是這家旅館的洗衣場。

（這裡給人的感覺，似乎連女性客人也能放心地使用。）

米魯茲旅館的洗衣場欠缺清潔感。
放眼望去，這家旅館的洗衣場晾著的衣物只有床單而已。
洗好的衣服大概都被晾在旅館房間裡吧。
沒有其他人的氣息。瑟拉絲帶著悠哉的心情，踏進洗衣場。
簡單準備好後，她從放置一旁的籃子中拿出衣服。
她迅速洗好自己衣物，接著再將手伸向掛在旁邊的男性衣物。
拿起衣服後──又暫時盯著衣服看了一會兒。

（這是登河大人的。）

雖然說是順便拿過來的，但其實並未經過本人同意。

（這麼一想⋯⋯）

這是她第一次清洗異性的衣物。
就在此時。
或許是一時鬼迷心竅吧？
瑟拉絲突然拿起登河的衣服──將高挺的小巧鼻尖埋入其中。
她輕柔地垂下眼簾，腦海裏浮現出衣物主人的身影。
隨後閉上雙眼，在心中呼喊他的名字。

（登河大人⋯⋯）

一股前所未有的不可思議感受，緩緩地縈繞在她體內。
仿彿就像是他的一切逐漸融入自己的感覺。

（光是味道──就能使人如此強烈感受到他人的存在啊⋯⋯）

一種奇妙的感情，短暫而強烈地停留在瑟拉絲心中。
那是一種交雜著安心和些微激動的不可思議感受。

（插圖０１３）

她緊閉的雙眼，突然睜開。

（──怎麼可能！）

瑟拉絲慌張地將臉龐從登河的衣服中移開。
她感受到兩邊臉頰愈來愈燙。心臟怦然跳動，脈搏不斷加速。

（⋯⋯我做了什麼？）

她左右環顧四周。

（沒有人看到吧⋯⋯）

胸中泛起安心的感覺。緊張的情緒緩和下來，綳緊的肩膀開始放鬆。
她將手背抵在嘴上，身體暫時呈現僵硬的狀態。

（我為何會這麼做⋯⋯）

她隱藏不住對於自己的驚訝。
就像受到吸引一般，莫名地聞起他衣服的味道。

（就算再怎麼有好感，我也不能⋯⋯）

厚顏無恥的行為。接著涌上心頭的，是自責的念頭。

（實在──既卑鄙，又卑劣⋯⋯）

就在那一瞬間──

「可以打擾妳一會兒嗎？」

驚訝之餘，瑟拉絲整個身體彈了起來。

「呀──非常抱歉！」
「⋯⋯為何要道歉？」

不知何時，登河居然出現在洗衣場。
心臟跳個不停，心音更是急速加快。

（什麼時候來的⋯⋯？登河大人是什、什麼時候開始在這裡的⋯⋯？）

眼神焦點不定，思考極度混亂。身體發熱，耳根子癢癢的。

「不好意思，我、我沒注意到你在場⋯⋯因為你突然從背後說話，我嚇了一跳。」
「抱歉啊。」
「啊，不⋯⋯⋯⋯那個，你來多久了⋯⋯？」

她心想：幸好我背對著他。
現在無法回頭直視他的臉──由於罪惡感和自責。

「嗯？啊啊，我才剛來。一過來就看到妳伸手抵著嘴巴，整個人僵在那裡⋯⋯是不是看到討厭的虫子？」
「──是、是的。」
「⋯⋯算了，我就不問妳事情的來龍去脈了。」

登河聳了聳肩。
剛才的反應──大概是謊言被他拆穿了。之所以不追問，想必是出自他的溫柔吧。

「對、對了，你找我有什麼事嗎⋯⋯？」
「因為我的衣服不見了⋯⋯所以來確認一下。」
「啊──非常抱歉，我雖然覺得太多管閑事，但還是把你的一起拿來洗了⋯⋯」
「那就好⋯⋯不過，連我的衣服也交給妳清洗，沒問題嗎？」
「請你別放在心上。我還欠你很多恩情沒還，而且洗衣服這件事我也習以為常了。」
「我也習慣自己洗了⋯⋯我們要不要輪流？不過⋯⋯讓男人碰自己的衣服，妳心裡可能也會抗拒，所以我覺得還是各自分開清洗比較好。」

她差點叫出一聲「登河大人」，但還是改變了主意。
目前陷入「兩人獨處」的尷尬氣氛。
登河也不敢叫出「瑟拉絲」三個字。

「如果是你──我一點也不介意⋯⋯」
「是嗎？那麼⋯⋯我們就先輪流吧。那樣比較公平。」

瑟拉絲頓時感到疑惑。
看來他對瑟拉絲的衣服毫無任何想法。
他果然不像自己一樣，會被意想不到的誘惑給吸引嗎？
該怎麼說呢⋯⋯看得出他的自制心堅定得驚人。

「那個，我有一件事情想要問你。」
「什麼事？」
「你對誘惑很有抵抗力嗎？」
「⋯⋯妳指什麼？」
「啊⋯⋯不是，那個⋯⋯」

瑟拉絲欲言又止。登河單手托住下巴，稍微想了一會兒。

「我想想⋯⋯現在說不定是很有抵抗力沒錯。」
「現在嗎？」
「對。」

登河咧嘴一笑。他笑裏藏刀的嘴角，露出潔白的牙齒。

「因為我現在心裡想的就只有復仇。很難將意識放在其他的事情──就某種意義上，就想被下咒限制住了吧。」
「下咒限制⋯⋯」
「不過，只要復仇成功，我或許就會去想其他的事。在那之前⋯⋯就像被詛咒了一樣吧。」
「那個詛咒──」

她將登河的衣服緊緊抱在胸前。

「為了能早一點將它解開，我也會盡心盡力幫助你。」

登河用鼻子哼了一聲。

「啊啊，那就拜託妳了。」

瑟拉絲帶著既靦腆又喜悅的笑容回應：

「是的，我的主人。」


◇【三森燈河】◇

瑟拉絲變身成身著精式靈裝的模樣。在旅館房間裡。
不管何時看，都覺得好像魔法少女變身的畫面。
當然，沒有演出用的動作就是了⋯⋯

為何瑟拉絲要在這種地方穿上一身精式靈裝呢？

「因為在這個狀態下，直覺會變得遲鈍。」

她如此說明。
她不想讓自己在大展身手時動作變遲鈍──大概是這樣吧。
房間裡只有我、瑟拉絲、嗶嘰丸三個人。
窗戶關緊，窗簾也全拉上了。
房門也上了鎖⋯⋯空氣有點悶。
幸好左右兩邊是空房間。
萬一有人過來，嗶嘰丸會告訴我。我也會加以注意。
因此，在房裡就不需要特別在意會不會被人發現真面目了。
瑟拉絲拔出劍，開始練習揮劍。

「練習揮劍」這詞彙，跟我印象中的動作感覺大大不同。
稱為之「表演」還比較適合。

華麗無比的劍技。我想她大概正在和腦海中的假想敵交手。
動作有緩有急。長髮隨著離心力飄起，衣服布料也隨之飄舞。
她本人很認真。
我靠在牆上，雙手環抱在胸前，望著她的模樣。
還是不要跟她說話好了⋯⋯不該去打擾她。

「──────疾！」

刀刃劃過空中。
斬斷的只是空氣。但是，她的劍刃彼端卻仿彿存在著某個擁有質量的物體。
應該說──動作整體上看來相當真實吧？

「嗯？」

一屁股坐（？）在我腳邊的嗶嘰丸──

「噗啾、噗啾、噗啾。」

居然跟隨瑟拉絲的動作，左右搖擺身體。

⋯⋯牠也在想像自己進行特訓的畫面嗎？

瑟拉絲揮劍的體態，用流暢華麗來形容也不為過。
行雲流水的劍舞，就算為她著迷而停下行動，也不會有人責怪。
由於我還是個門外漢，無法說得頭頭是道，但她運用空間的方式簡直無可挑剔。
室內空間勉強還算寬闊，但她居然能在有限的範圍內行動自如。
白色肌膚滲出汗水，不時彈飛出來。揮灑出來的汗水，甚至彈到我臉頰上。
她的專注力著實驚人。
我邊用袖子擦掉濺到臉上的汗水，邊眺望著瑟拉絲的動作。


⋯⋯不愧是屍人賦予期待的敵人。

這和她之間的差距，就算用活動狀態補正值也無法填滿。
長年培養出來的戰鬥技術。這是我所欠缺的。
就在瑟拉絲身體散發出的熱氣讓我覺得有點熱的同時，自主特訓就此結束。
見她將劍靠在牆上放好后，我將手中的白布交給瑟拉絲。

「給妳。」
「啊──不好意思，謝謝你。」

微燙的白皙頸子，浮現出晶瑩剔透的汗珠。
瑟拉絲撥開黏在脖子上的頭髮，以手中的布拭去汗水。

「──呼。」

瑟拉絲調整呼吸。

「妳能不能利用冰之精靈的力量降低房間的溫度？」

瑟拉絲苦笑。

「也不是做不到。」
「我知道。只是開個玩笑。」

精靈之力。可以的話，最好不要使用。
除了會讓她付出想睡的代價之外，對瑟拉絲本身也會造成負擔。

「這次就此告一段落嗎？」
「對。我只是想確認一下自己的身手有沒有變遲鈍而已。」
「那個⋯⋯關於近身戰，妳有沒有什麼簡單的訣竅？」

擦拭臉頰的瑟拉絲，表現出明白我用意的反應。

「你對近身戰有興趣嗎？」
「但是，如果要實際對打的話，就得經常進行特訓，不是嗎？」
「是這樣沒錯⋯⋯因為不是一朝一夕就能練出來的。」

現在的我在近身戰時，感覺都是靠活動狀態在硬來。

「太依賴技能也不好⋯⋯我覺得在緊要關頭時，說不定會用得到近身戰技巧。」
「──我知道了。」

瑟拉絲將布折好放在床上後，朝我走過來。

「拿著武器的方式，還有拿武器時擺放重心的位置⋯⋯先學這些比較適合你吧。」

瑟拉絲露出淡淡的微笑。

「怎麼了嗎？」
「沒有⋯⋯我擔任聖騎士團長時，也曾這樣指導過騎士團員，頓時讓我懷念起往事了。」

全由女性組成的那支聖騎士團嗎？
也對，瑟拉絲看起來很會照顧人。想像得出來，她相當受人愛戴。
沉浸在回憶之後，前聖騎士團長表情變得緊繃。

「那麼──」

瑟拉絲雙腳並攏，站在我面前。

「請你襲擊我。」
「──我知道了。」

我想這大概是她要教我遭到襲擊時應對的方法吧。
此時──

「啊，不是的，那個──」

瑟拉絲連忙補充。

「我說的襲擊，是『使用暴力來打倒我看看』的『襲擊』⋯⋯」
「用不著妳說我也知道。那麼，我要開始囉。」
「⋯⋯！────好，隨時都可以。」

瑟拉絲再次綳緊神情。

⋯⋯光看她的眼神和表情，我就能感受到些許氣勢。

瑟拉絲跟他人敵對時就會變成這樣。

「請你全力攻打過來。攻擊目標在我左臉頰⋯⋯若打中了，我想多少會有些紅腫。關於這一點，請別擔心。」

我點頭。然後──用力踩踏地板，向前一步準備出拳。
我扭動腰部，連續出拳。
速度夠快，但力量根本不夠。
但是，若被擊中臉頰，多少會腫起來吧？
雖是訓練，但還是令我怯步──

抓住！拉扯！

「────、⋯⋯⋯⋯！」

我揮出去的拳頭，轉瞬之間就被她抓住並往上扭轉。
剛才還站在我面前的瑟拉絲，現在已經站在我身後。
我從手腕到肩膀，都隱隱作痛。

「這麼做就能用最低限度的動作，封鎖住對手的行動了。」
「⋯⋯真厲害啊。」
「如果還想進一步封鎖對方的行動，就要像這樣──」
「嗚呃⋯⋯！」

瑟拉絲的身體從背後緊貼過來的同時，我隱隱作痛的感覺變得更加強烈。
原本行動自如的左手也被一把抓住，扯向背後。
接著，左手被往上扭轉，同時感受到扭曲的疼痛和壓迫感。
如此一來，雙手都被完全封死了。即便我使出全力試圖掙脫──

「力量使不上來。」
「沒錯，會變成這樣。」

不可思議的是，連腰都使不上力。
這就是在不傷害對手的情況下，令其無力的技巧嗎？大概比較接近格闘技吧？
瑟拉絲小小喘了口氣，調整呼吸。

「若是這個技能，既不需要武器，當你不願給予對手致命傷害時也可以使用。」

她的語氣變得溫柔了一些。原來如此。

「就算活動狀態提升了，我還是很難學會這招呢。」
「某種程度上需要反覆練習⋯⋯只練習這個的話，若由我一個個步驟親自指導你，只要短短幾天⋯⋯就、能⋯⋯！」

啪！
瑟拉絲突然解開對我的束縛，往後跳開。

「不好意思──在汗流浹背的情況下，還這麼緊貼著你。」

瑟拉絲輕輕聞一下自己的上臂。

「⋯⋯距離這麼近，你可能會聞到我的汗臭味。」

後半句聲音愈來愈小。
我以為她是故意忽略這點來指導我⋯⋯

看來只是太遲鈍了而已。
而且瑟拉絲的體味並不重⋯⋯也不是令人不快的味道。只是──

「如果妳排斥身體緊貼的話，口頭教我就可以了。」
「登、登河大人覺得像剛才那樣的指導⋯⋯沒問題嗎？」
「我是無所謂啦。我不在意那些小細節，只想認真學習。倒是妳呢？」

在這種情況下，比起我，反倒是確認一下瑟拉絲的想法更為重要吧。

「我當然也沒問題⋯⋯」
「那就好了。那麼──可以繼續嗎？」

既可以免費學到戰鬥技術，又可以在短期間內學會基礎。
如此千載難逢的好機會可不多。我應該善加利用。
瑟拉絲放下心後，調整打亂的呼吸。

「那麼，我們學下一招吧。」

我也重振精神。

「──好，拜託妳了。」

───

現在，瑟拉絲正在床上就寢。

規律的呼吸聲。她的胸部上下微微起伏。
睡臉非常平靜。
剛才我在瑟拉絲身上施加【SLEEP（睡眠性賦予）】

她並不是因為精靈的代價而感到身心俱疲，但我能看得出來今晚的她顯得特別疲倦。
所以，我提議使用【SLEEP（睡眠性賦予）】讓她陷入沉睡。
我觀察她疲勞的程度，使用技能讓她入睡。她肯讓我這樣做就已經很誇張了。但是──

「⋯⋯⋯⋯」

對於我要讓她睡著這件事，她一點戒心都沒有。
施予技能讓她入睡後，只要我不解除，她就絶對不會醒過來。
不管對她做什麼，她也不會被吵醒。
瑟拉絲明明也知道這一點。

⋯⋯那個血闘士，說我是一個濫好人。

「嗯、嗚！」

我一邊凝視著翻來覆去的瑟拉絲，一邊碎碎唸。

「貨真價實的濫好人，就是指像她這樣的人啊。」

濫好人。
腦海中馬上浮現出叔叔和嬸嬸的身影。
接著浮現的是同班同學。
我隔著窗簾，朝外頭遠遠望過去。
鹿島小鳩和十河綾香。
她們兩個現在怎麼樣了呢⋯⋯？

我在那之後，便一直保持清醒。
夜已深。我坐在地上，拿著《禁術大全》一頁一頁翻閱。
旁邊放著製作禁術道具用的器具。燈光只有小型煤油燈。
此時，感覺身後傳來衣服摩擦的聲音。
原來⋯⋯已經到了【SLEEP】失效的時間嗎？

「你還沒睡啊，登河大人？」

我已經很小心不發出聲音了。

「抱歉。把妳吵醒了嗎？」
「啊，沒有⋯⋯我並不是因為聲音或光線而醒來的。那個──」

瑟拉絲欲言又止。聲音聽起來有所猶豫。

⋯⋯啊啊，原來是這樣。

我轉過頭來。

「妳什麼話都不用說。還有──」

瑟拉絲用手挪著身子在床單上做出起身動作，她穿得很單薄。
油燈微微的暖光，緩緩照映在她裸露的身軀上。
在她大腿的旁邊，可以看到她脫掉的上衣揉成一團。
我的視線再度回到《禁術大全》

「出發前記得變成可以偽裝的模樣啊。另外⋯⋯也不要忘了改變妳的臉。」
「──啊，不好意思⋯⋯是的，我會提醒自己。」

持續傳來匆忙急促的衣服摩擦聲。看來她正倉皇失措地在整理要穿的衣服。
瑟拉絲不是故意要展現裸露的姿態，但她仍感到難為情。
似乎是因為剛起床而精神恍惚的緣故，才沒能顧慮到自己的打扮。
我翻了下一頁，並說道。

「果然還是分房睡會比較好嗎？」
「不，答應睡同一寢室的人是我。像這樣的，那個⋯⋯對於會發生這種始料未及事態的可能性，我也有所覺悟了。而且⋯⋯剛才說來慚愧，不知該說是我恍神呢？還是一時疏忽而惹禍──」
「瑟拉絲。」
「是、是的。」

我指著門的位置。

「我大概猜得到妳起床的理由，那個⋯⋯妳不用出去一趟嗎？」

⋯⋯還是暫時不要直接了當地去點破她好了。

「啊────」

瑟拉絲一下床後，表情就產生變化。
接著不知為何跟我行禮後，就快步離開房間。
腳步聲愈來愈遠。
房間終於恢復寧靜。目前嗶嘰丸也很聽話。

啪啦！

連翻頁的聲音都能聽得相當清楚。

「⋯⋯⋯⋯」

讓人感到不可思議的是，在寧靜的夜晚，心靈顯得特別平靜。以前，晚上我總是因為不安而睡不著覺。
也是因為跟叔叔他們一起住後，才能整晚都不會產生痙攣。

⋯⋯說到痙攣，瑟拉絲應該也跟我一樣。

身為逃亡者過著隱居生活。
截然不同的一點是，她的名字和臉都眾所皆知。
說不定就跟有名歌手或藝人隱居時一樣。
我注視著瑟拉絲的床，被單整個都捲起來了。
她似乎比獨自旅行的時候更能安穩入睡了。當事人也這麼說道。

「──蒼蠅王和公主騎士，是嗎？」

主從關係。
正因為有主從關係的立場，所以更不該帶給她不必要的沉重壓力。
反而主人必須要做的是，成為賦予她安定的存在。
反而主人必須要做的是，成為促進她奮發的存在。
然後，三森燈河必須要做的就是，飾演一個主人的角色。
我最擅長的就是演戲。

「⋯⋯而且。」

正在翻頁的手停了下來。
總覺得⋯⋯瑟拉絲也在努力扮演好自己的角色。
她盡心地要成為我的「劍」

帶著強烈的意識去飾演一個「隨從」的角色──她給我的感覺是這樣。

「對我來說，是很幸運的事啦⋯⋯」

但另一方面，我也知道瑟拉絲在隱藏真心。
我凝視著她睡過的空床。
我對著四下無人的空間說道。

「謝謝妳願意壓抑許多情感跟我相處。」

視線回到書本頁面上。
過了一會兒，瑟拉絲回來了。
她進到房間裡後，手伸向後方將房門輕輕關上。

「登河大人，你還不睏嗎？」
「嗯？」

瑟拉絲故意輕咳了一下。

「跟我說明睡覺有多重要的人，可是你喔？勸說過我的當事人若睡眠不足，再怎麼樣也太不像話了吧？」

我翻著下一頁。

「如果感覺到需要睡覺的時候，我一定會好好睡⋯⋯不管是何種生物，睡眠不足都是一種罪惡啊。」

反過來說的話，熟睡才是正義。關於這一點，正義和邪惡可以說是有絶對關係。

「雖然是這樣說啦，還是會有睡不著的夜晚呢⋯⋯」
「──我懂。」

瑟拉絲點頭表示同意，之後往她放行李的位置走去。
她翻找行李發出窸窸窣窣的聲響。隨後回來，在我身旁屈膝坐下。
有一些小瓶子在她手中。可以看到裡面刻印著像葉子一樣的東西。
瑟拉絲打開蓋子後，將裡面裝的東西放在她的手心上。

「這是屬於藥草的一種。」
「可以比較好入睡，之類的？」
「呵呵，就是你想的那樣。」
「藥草有好多種類啊⋯⋯而且顏色也不太一樣。」

瑟拉絲用另一隻手拿起別的瓶子。

「是的。這種藥草顏色跟它很像，但跟另一種又是完全相反⋯⋯據說吃了這藥草，具有兩種效果，既能變得有精力，同時又能提升氣勢。」

一說完，瑟拉絲看似很尷尬地低下頭來，開始在反省自我。

「──恕我失禮了。如果讓你不開心的話，非常抱歉⋯⋯那個，我絶對沒有其他意思⋯⋯」
「我知道。而且我沒有不開心。話說回來──」

藥草，是嗎？

「如果跟那個混在一起喝，說不定還不錯。」

我從皮囊裡拿出清湯粉。
這是在旅途中從魔法皮囊傳送過來的東西。
由於可以保存，所以我想剛拿出來的清湯粉應該沒問題。

「妳等我一下。」

我站起身來，到共用樓梯下的廚房裡開始煮熱水。
我用剛借來的鍋子熱湯，將裡面的湯頭盛到碗裡。
接著再將清湯粉倒進去，並用木製湯匙將它攪勻。
瑟拉絲吞了吞口水。

「味道真香⋯⋯」
「雖然看似普通，但這個真的很好喝喔。」

熱氣傳到鼻子裡，散發出獨特的香味。
不知道是不是因為在這種時間聞著湯頭的緣故，反而激起了食欲。

⋯⋯但瑟拉絲也未免太興奮了吧。

「在把藥草倒進去以前，妳要不要先嘗一口看看？」
「──請務必讓我喝喝看。」

瑟拉絲喝了一口湯。喉嚨傳出輕快的聲響，湯就這樣進到她的胃裡。
瑟拉絲的手摸著臉頰，自言自語似地不斷說著「真好吃」

「調味簡直絶妙無比。」
「絶妙嗎？」
「是的，絶妙。」

瑟拉絲好奇地拿起包裝袋。

「只要將這種粉倒進熱水裡，就能做成湯頭嗎⋯⋯無論是做為旅途用，還是戰場用的攜帶糧食，都挺合適的。」
「話說回來⋯⋯差不多能拜託妳調配藥草了嗎？」
「啊──是的，我現在就去調。」
「有什麼需要幫忙的嗎？」
「如果只是調配這個的話，我一個人也沒問題。」

瑟拉絲秤著適當的份量，把藥草撒進去。

「啊──糟了。」
「⋯⋯⋯⋯」

因為沒控制好力氣，在第一杯時不小心發生了倒進太多的意外。

「好了。」
「⋯⋯搞錯的那一份，要不要由我來喝掉？」
「不，這一碗我來喝。請務必讓我負起這個責任。」
「我知道了。」

總而言之，我喝下適量的那一碗。
加過藥草的湯。我將碗湊近到嘴邊，輕輕地喝了一小口。

「滋滋⋯⋯咕嚕──」

有種辣辣的感覺。大概跟花椒的味道有點像吧？

「因為份量不對，味道可能會有點重⋯⋯但果然喝下這個後，會有一股清爽的感覺呢。」

喝完湯後，我一手拿著碗，皺起眉頭。

「清爽的感覺？」

我們兩人的感想完全不一致嗎？

「登河大人的感覺不同嗎？這藥草的話，一般來說是⋯⋯──！？」

瑟拉絲臉色發青。她立刻拿起小瓶子來確認⋯⋯該不會──

「妳搞錯了嗎？」
「是的。」

沒想到能幹的瑟拉絲居然會犯了這種大家都會犯的錯誤。
瑟拉絲搞錯了第一杯的份量。
接著是第二杯。就在第一杯失敗後，她馬上動搖了。
受到動搖的影響，她竟然完全不確認一下手中的瓶子，就把內容物加進去啊？

「登河、大人。」

瑟拉絲帶著擔憂的眼神，將手放在胸上看著我。
呈現出一副對不起我的氣氛。

「那個，真的非常不好意思⋯⋯我把跟助眠效果完全相反的藥草⋯⋯」
「不要緊。多虧如此，今晚我似乎可以看這本書久一點了。」

我試著翻開《禁術大全》⋯⋯沒錯，我相當逞強。
心臟的脈動異常加速，身體也好熱。神經都處在一種亢奮狀態。

⋯⋯這下傷腦筋了。

心完全靜不下來。

「心靜不下來的話，那個──有沒有我能為你做的事情？」

妳是因為知道我的狀態才這樣說的？
還是什麼都不知道才這樣說的？
我無法判斷。
算了⋯⋯無論如何，都不用瑟拉絲幫忙。

「妳去睡吧。明天要去蒐集情報。」
「但是，登河大人你⋯⋯」
「我也會小睡一下。總之，我要再使用一次【SLEEP（睡眠性賦予）】了。」

瑟拉絲說了一聲「那就麻煩你了」，乖乖地回到床上。

「在我看來，好像一直在給你添麻煩⋯⋯那個，真的沒問題嗎？」

瑟拉絲保持仰臥的狀態，瞇著眼問我。

「⋯⋯我沒問題。好了啦，妳快睡。」
「──是的。」

我賦予她【SLEEP】後，終於能聽見有規律的呼吸聲了。
順帶一提，雖然規定上是不可以連續賦予同一對象，但只要有間隔冷卻時間就沒問題。
由於當時在廢棄遺跡時是失效後立即使用的，所以才會像是沒辦法連續賦予。
確認瑟拉絲已睡著後，我開始看《禁術大全》的後續。

「嗶啾～」

一直一語不發地在房間角落待命的嗶嘰丸，朝我這裡過來。
牠一跳，彈到我肩膀上。

「嗯？怎麼，你也睡不著嗎？」

我也搞不清楚牠到底有沒有睡覺的概念就是了。

「那麼，我何時才會想睡覺呢⋯⋯」

徹底熬夜的話，那也未免太勉強了。
我一度將《禁術大全》闔上，仰望著天花板。

「話說回來，叔叔好像有說過。只有年輕的時候，才有辦法勉強自己。」
「嗶呢～」
「這麼說來，你幾歳了啊？」
「嗶嘰？」
「這樣啊，你不知道啊？」
「嗶嘰。」
「⋯⋯嗯。」

於是──嗶嘰丸變形後，開始按摩我的一側頭部、肩膀、腰部。
感覺變得舒服起來了⋯⋯交感神經亢奮的程度也緩和許多。
牠察覺到我因睡不著而困擾的模樣，似乎想要讓我放輕鬆。

「嗶嘰丸，你啊⋯⋯不管怎麼說，也未免⋯⋯太可靠了吧⋯⋯」
「嗶嘰～♪」

該說是史萊姆式按摩的功勞嗎？我比預料中更早入睡了。

───

隔天早上。

「我換好衣服了，登河大人。」

瑟拉絲在我身後換好衣服。

「那麼，我們出發吧。」

我摸著嗶嘰丸。

「今天也交給你看家囉。」
「嗶！」

今天先去血闘場。為了蒐集伊芙・史畢德的情報。
我們在旅館的一樓吃完早餐後，就到外面去了。

「⋯⋯嗯？妳怎麼了？」

瑟拉絲拿自己沒輒似地縮起肩膀。

「非常不好意思，我吃得太慢了。」
「別介意。比起這個，妳睡得還可以嗎？」

瑟拉絲的臉部變化能力是借用精靈之力。
相對的代價就是睡眠欲。
昨晚我在瑟拉絲身上施予【SLEEP】

因等級提升，所以增加了持續時間。現在，效果最多可以持續到三小時為止。

「我三小時後就起床了。」
「一直醒到早上？」
「是的，因為必須支付代價。但是，在還清以前，比起每一天完全無法熟睡的狀況，目前已經好上好幾倍。」

的確，聽她的聲音，感覺比以前還要健康許多。
瑟拉絲一直以來在旅途過程中，都沒有適當的睡眠時間。
在償還代價以前，都難以入睡。
然而，現在我利用【SLEEP】來縮短時間。這就有了很大的差異。
雖然有點作弊的感覺就是了。

「畢竟妳當時在米魯茲遺跡就一副軟弱無力的樣子了嘛。」
「⋯⋯那、那次真的讓你擔心了。」
「不過仔細想一想，妳在那種狀態下還可以靈活地行動，不是嗎？反而要好好誇獎妳擁有堅強的意志力，對吧？」

瑟拉絲苦笑了一下。

「主人總是都在稱讚我呢。」
「妳基本上是誇獎就能成長的類型嘛。」

我們抵達血闘場。這地方是大街旁的一個地區。
看到血闘場，果然最先聯想到的是⋯⋯古羅馬競技場嗎？
就是在古代羅馬系列的故事中很常看到的那個。
我找了一下場外公布的內部地圖。
觀眾席呈現團團包圍競技場中心的形狀。
這個形狀，無論在什麼場景出現都不會產生變化嗎？
血闘場外的廣場人來人往。
場內一陣歡呼聲傳到空中。今天似乎也在血闘中。

「接下來，該怎麼做才好呢⋯⋯今天伊芙・史畢德的血闘在──」
「伊芙・史畢德好像沒有血闘行程。」

瑟拉絲探頭看了一下布告欄的內容，如此說道。

「這樣啊。」

從現狀來看，觀望血闘之爭好像也沒有什麼幫助⋯⋯

「總之，我們就先跟那裡的人打聽一下伊芙・史畢德的評價，或是一些奇聞軼事吧⋯⋯在沒有網路的世界裡，能深深體會這種不方便的感覺啊。」

正因為如此，所以才更加慶幸有一個如瑟拉絲這般知識淵博的存在。

「網路是什麼？」
「嗯？啊啊，在我待過的世界裡有一種方便的手段，能閱覽全世界的資訊喔。不過好像還無法閱覽最高機密之類的資料就是了。」

雖然如此，基礎的情報，在網路就已經能蒐集到很多了。

「那東西就跟誰都能自由出入的書庫是一樣的嗎？」
「好像不太一樣。」
「在這裡的話就無法使用網路了，對吧？」
「看來是這樣。」

智慧型手機也沒開機。
我望著人來人往的光景。

「我們暫且去強化基礎情報吧。」

對豹人興致勃勃的王都新手鄕巴佬──差不多偽裝成這樣吧。

「我們分頭蒐集情報嗎？」
「說得也是。時間寶貴。」

昨晚我分了一袋硬幣給瑟拉絲。

「如果有需要的話，這些錢就靠妳自行判斷要不要使用。有的人只要請他吃一頓飯，話匣子就會打開了。」

正好在血闘場前也有路邊攤。

「⋯⋯這樣好嗎？」
「這也就是所謂的『經費』吧。不，其實我對經費也不是很了解。」

以前，叔叔曾講過經費的事。
在工作上使用的金錢就是稱為經費。我的理解大概也只有這樣。
這到底算不算得上「工作」，還挺難說的。
我們開始蒐集關於伊芙的情報。
大約過了三小時以後，我們再度到血闘場前會合。

「有一段時間都沒看到妳呢。」

從途中就不見瑟拉絲的踪影。

「其實我特地跑到更遠的地區去了。」
「這樣啊。」

我交給瑟拉絲自行判斷。她個性負責、耿直，不像會摸魚。

「所以，結果如何？」
「該說不愧是人氣高的血闘士嗎⋯⋯只要一提到伊芙・史畢德的事，一堆人都說個不停呢。」

我也碰到同樣的情況。
大家都欣喜若狂地談論伊芙・史畢德到底有多強。
人們只要提到自己喜歡的事，就會爽快說出來。多虧如此，情報很好蒐集。
我們將成果拿出來互相比較、總結。

伊芙原本好像是個奴隷。據說她是被奴隷商賣到血闘場的。
再來──伊芙被賣過來的時期，當時客人們也對血闘感到膩了。
因此，負責營運的單位，便投入來自少數種族的豹人參與血闘。
結果，她從第一戰就將場子炒得盛大無比。
血闘就像是被注入生命一樣重生了。

「這三年來，沒有一次吃過敗仗，還真是厲害啊。」
「因為只要有伊芙在的陣營，就一定會獲勝，所以聽說從半年前開始，她就不被准許再參加團體戰了。」
「也就是說，團體戰的勝敗關鍵，是因為伊芙個人能力很高嗎⋯⋯不僅如此，印象中，伊芙的支持者相當多。幾乎沒有人期待她戰敗。」

同一個參戰者一直打贏，觀眾會看膩，但伊芙的情況似乎有點不同。

「想必她有特別留意能吸引到觀眾的戰術吧。她大概很熟悉觀眾期望的獲勝方式是什麼。她給我一種印象，就是大家都是為了看她打贏才前來觀戰的。」

所以才這麼廣受歡迎，是嗎？

「妳有聽說明天伊芙・史畢德要參加最後一場血闘的消息嗎？」
「是的，我手中的情報也跟你一樣。」
「最後的血闘，代表她已經幾乎賺到能換取自由之身的金額了嗎⋯⋯這樣的話，提出金錢交易，她可能未必會接受。」

她也不像見錢眼開的類型。

「不過，有一個情報讓我滿在意的。」

瑟拉絲身體靠了過來。她呢喃細語地繼續說道：

「據說她早在兩年前，就已經獲得了可以贖回自己的金額。」
「嗯？那麼，後來的這兩年，她是在賺取自由後要花的錢嗎？」

那也很有可能。表示她的儲蓄一定也相當充足。

「然而，事實卻不是如此。」
「什麼意思？」
「她似乎打算要贖回自己以外的人。」
「⋯⋯所以她才要在兩年期間，多賺一點額外的費用嗎？她到底要贖回哪裡的誰──我想妳應該不知道吧？」

瑟拉絲遞出一張紙片。

「請看這個。」

我拿著紙片確認內容，上面記了一些簡單的情報和名字。

「小孩子？」
「她是跟伊芙同一個時期，在蒙洛伊被奴隷商賣掉的孩子。」
「她是伊芙的女兒嗎？」
「不是，她似乎不是豹人。」
「喔～？」

內情似乎有些複雜。

「無論如何，我們能用金錢打動她的機會，可說是少之又少⋯⋯」

我回想起當時跟伊芙談論禁忌魔女的話題。
那種感覺⋯⋯她雖然為人爽快，但卻給人一種頑強的印象。
或許禁忌魔女有恩於她。
若要讓她說出實話，看來我必須給她更大的恩情才行嗎？
我心裡盤算著。

「那麼，要利用什麼交易呢⋯⋯」

贖回剛才提及的那女孩的資金，伊芙早已經賺到了⋯⋯

這下子只能忽略伊芙，直接進入魔群帶了嗎？
或者是──僱用自由之身的伊芙，來當我的護衛嗎？

「那個，我還有一件事情想要告訴你。」
「⋯⋯⋯⋯」

瑟拉絲的情報量是怎麼回事？我蒐集到的數量，根本無法與她相比。

「在那之前，我可以問妳一件事嗎？妳那些情報，都是怎麼來的⋯⋯？」

瑟拉絲猶豫地回答。

「我利用了情報販子。」
「情報販子？」
「雖然在米魯茲無此管道，但像蒙洛伊這樣大規模的都市，就會有處理地下情報的情報販子。」

所以她才去了其他地區啊？
知道可以拿青龍石典當的地方時也是，她的這一點真是不容小覷。
這麼說來，她曾經當過騎士團的團長啊⋯⋯

莫非她平常就常利用情報販子來獲得地下情報嗎？

「關於『陰暗處』的事，我是跟公主學來的。我也聽說，宮廷內曾有過人人處心積慮、勾心鬥角的時代。據說公主巧妙利用了地下世界的人，來保護自己。」

負責經手骯髒工作的地下世界。
在這個世界似乎被稱為『陰暗處』

原來如此。瑟拉絲如此精通的原因，就是受到她效忠的公主影響啊。

「關於我一聲不吭獨自跑去找情報販子，我非常抱歉。只不過，因為他們極端討厭委託者帶著不認識他們的同行者找上門⋯⋯」
「我都讓妳自行判斷了，妳不用這麼介意。順便一提，他們是怎麼判斷來者認不認識他們的？」

我純粹感興趣地問道。

「我知道他們的『規矩』。這個『規矩』若非情報販子的『客人』，是不知道的。那些『規矩』是身為『客人』的證明。不懂裝懂的『規矩』，他們一下就輕易識破了。」

就像知道的人才會懂的暗號嗎？身為一個必須貫徹機密的集團，很像他們的作風。
我也能了解瑟拉絲獨自前往的理由了。我根本不知道所謂的『規矩』

如果我跟著瑟拉絲，說不定反而會讓情報販子對我產生過度的警戒心。

「但是，我至少應該要告訴你，我利用了情報販子⋯⋯非常抱歉。還有這個──」

瑟拉絲誠惶誠恐地拿出一樣東西。是一個空的小袋子。

「你給我的錢，我全部都拿去花在取得情報之上了。畢竟⋯⋯跟他們殺價，不是很恰當。」
「那樣一點問題都沒有。多虧妳，我才能獲得如此貴重的情報。我反而要感謝妳呢。」

瑟拉絲鬆了一口氣。

「主人果然很溫柔呢。」

⋯⋯正常來想，根本就沒有斥責她的道理吧？

「所以，妳還要告訴我的另一件事情是什麼？」
「是的，關於那件事──」

瑟拉絲露出玄妙的神色。

「過去被譽為最強的血闘士，多半都在決戰中死亡了，這件事你知道嗎？」
「啊啊，那件事我也聽說過。」

最後一場對決大多都會吃敗仗。正因為如此，總是特別熱鬧。
是活是死？
機率超低的生存率。仿彿就像廢棄遺跡。

「不過，正因為是最後一場對決，所以只要擊潰難以對付的對手就好了不是嗎？」

如果只是那樣的話，感覺也算不上什麼地下情報。然而──

「看來，不僅如此。」

瑟拉絲觀察了一下四周。接著，她更加靠了過來。
她稍微踮起腳尖，將嘴巴湊近我耳邊。

「其中一個經營血闘場的公爵家，會在對決之日當天做出對血闘士不利的行為。」

那絶對不是值得讚賞的行為。
不，可以說是卑鄙無恥。是一種踐踏著血闘士希望的不當行為。
即使如此，我居然將那件事──

「哼嗯？」

視為一個好機會。我深深覺得自己是個人渣。

「突破口說不定就在那裡。」

瑟拉絲歪著頭，臉上浮現出問號。

「但是，為何要在光榮的最後一場血闘中，暗地做出那種事呢？」

最後的血闘，在當紅血闘士獲得勝利中畫下休止符。
無庸置疑的快樂結局──沒錯，對血闘士而言是如此。
然而，對於那些愛好血闘的觀眾們來說，又是如何呢？

「能招攬觀眾的血闘士離開後，就會需要下一個人氣王。那麼⋯⋯想在一夕之間打造出人氣王，妳覺得怎麼做比較有效果？」
「啊！」

看來瑟拉絲也想到了。

「沒錯。殺死當紅血闘士的人，就能一鼓作氣衝上『下一個人氣王』的位子。」

最完美的「世代交替」
營運負責人想要準備好下一個招觀眾的人（新英雄）

利用了人氣王的最後一場血闘。

「至於在招攬觀眾方面沒有特別成效的傢伙，一定是在他們獲勝之後，就給予他們自由了吧？」

有人獲得了自由之身。
可能性不是零。這一點很重要。
數量雖少，但仍有成功的案例。
名為『希望』的贋品就此誕生。

「只不過，伊芙・史畢德是百戰百勝的王者。她深受歡迎。如果想製造出一個能超越她的人氣王──」
「可以確定營運負責人一定會在最後的血闘中殺死她⋯⋯為了製造下一個人氣王。」
「正是如此。」

殺死伊芙・史畢德的人，可獲得不敗殺手的稱號。
隔天就能坐在伊芙的寶座上。
瑟拉絲露出一副想不通的神情。

「但是，她深受嗜好血闘的市民歡迎吧？大家就是為了看她獲勝的英姿，才特地前來觀賞⋯⋯既然如此，民眾期望的，應該是最後拿下勝利並重獲自由的伊芙・史畢德，不是嗎？」

原來瑟拉絲的人類觀，是這樣的啊？

「很遺憾，我認為妳錯了。」
「這、這是什麼意思？」
「聚集在血闘場中的觀眾，喜歡的是『能取悅他們的伊芙・史畢德』。我覺得觀眾並不是特別喜歡伊芙・史畢德本人。」

雖然這兩句話很像，但截然不同。
如果伊芙・史畢德不是血闘士，對他們而言，幾乎等於毫無價值。
觀眾們只是想看伊芙的『血闘』而已。

「原、原來如此⋯⋯若是嗜好血闘的觀眾，我想你說得沒錯⋯⋯」

喜歡作者創造出來的作品，但並不是喜歡作者本人。
不創作的作者就等於毫無價值。也讓人對他不感興趣。
我想大概就跟這樣的價值觀很相近吧？
我跟幾乎如膠水等級般貼著我的瑟拉絲稍微拉開距離後，切換到下一個話題。

「不過，最重要的不是那個。」

最重要的只有一點。

「首先，負責營運的公爵，明天一定會在伊芙的血闘中，設下陷阱。」
「也就是說，我們明天要去阻止他嗎？」

瑟拉絲似乎很起勁。大概是因為心境上偏向伊芙那一邊吧？

「不，我不打算這麼做。」
「咦？」

我看著血闘場。

「妳太客氣了──我們用不著特地等到明天吧？」